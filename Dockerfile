#FROM wordpress:5.0.3-php7.1-fpm
FROM wordpress:latest
COPY ./wp-content/themes/. /var/www/html/wp-content/themes/
