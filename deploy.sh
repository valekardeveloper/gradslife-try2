echo "Recreating project folder...."
rm -rf project
mkdir project
cd ~/project

#docker rm -f $(docker ps -aq)

################################# get the projects #########################################
#uncomment below if deploying for the first time
echo "Cloning nginx-proxy"
rm -rf nginx-proxy-companion
git clone https://valekardeveloper@bitbucket.org/valekardeveloper/nginx-proxy-companion.git

echo "Cloning Grafslife project ...."
rm -rf gradslife
git clone git@bitbucket.org:valekardeveloper/gradslife.git
##########################################################################################


############ start the proxy ############################################################
## uncomment below if deploying for the first time
echo "starting the proxy container"
cd nginx-proxy-companion
mv ../gradslife/nginx-deploy.env .env

'''# Start the network
network_name=${NETWORK:-webproxy}
network=$(docker network ls | awk -v network=$network_name '$0~network')
if [[ -z "$network" ]]; then
    docker network create $network_name
	echo "Started network"
else 
	echo "Removing network...."
	docker network rm $network_name
	docker network create $network_name
	echo "Started network"
fi'''

sh start.sh
cd ..


############################################################################################
echo "Instantiating containers...."
cd gradslife

##############################Remove existing wordpress container#####################

echo "Removing existing wordpress containers...."

wordpress="wordpress"
x=$(docker ps -a | awk -v word=$wordpress '$0~word' )
container_ID=$(echo $x | awk '{print $1}')
image_name=$(echo $x | awk '{print $2}')
echo $container_ID 
echo $image_name
#remove docker container

if [[ ! -z "$container_ID" ]]; then
	docker stop $container_ID
	docker rm $container_ID
	#remove docker image
	docker rmi $image_name
fi



#########################################################################################
#create the containers
#docker-compose up --force-recreate -d
docker-compose up -d 
##############################################################################################


echo "Deploy script finished execution"


